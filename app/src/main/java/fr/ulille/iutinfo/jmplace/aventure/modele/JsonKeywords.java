package fr.ulille.iutinfo.jmplace.aventure.modele;

public interface JsonKeywords {
    String KEY_ETATS = "pages";
    String KEY_TITLE = "title";
    String KEY_ID = "id";
    String KEY_DESCRIPTION = "description";
    String KEY_CHOIX = "choix";
    String KEY_LIBELLE = "libellé";
    String KEY_NEXT = "next";
    String KEY_CONDITION = "condition";


}
