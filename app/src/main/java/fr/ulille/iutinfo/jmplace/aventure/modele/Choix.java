package fr.ulille.iutinfo.jmplace.aventure.modele;

import static fr.ulille.iutinfo.jmplace.aventure.modele.JsonKeywords.KEY_CONDITION;
import static fr.ulille.iutinfo.jmplace.aventure.modele.JsonKeywords.KEY_LIBELLE;
import static fr.ulille.iutinfo.jmplace.aventure.modele.JsonKeywords.KEY_NEXT;

import org.json.JSONException;
import org.json.JSONObject;

import fr.ulille.iutinfo.jmplace.aventure.game.Game;


public class Choix {
    private String libelle;
    private String next;
    private Action action;
    private Condition condition;

    /**
     * Construit un choix à partir de sa description JSON
     * @param json La description
     * @throws JSONException Si la description est incorrecte
     */
    public Choix(JSONObject json) throws JSONException {
        libelle = json.getString(KEY_LIBELLE);
        next = json.optString(KEY_NEXT, "NO_LABEL");
        try {
            action = new Action(json.getJSONObject("action"));
        } catch (JSONException e) {
            action = null;
        }
        try {
            condition = new ConditionFlag(json.get(KEY_CONDITION));
        } catch (JSONException e) {
            condition = null;
        }
    }

    /**
     * Test si le choix est actif (selon le contexte du jeu)
     * @param game le contexte du jeu
     * @return vtai si le choix est actif
     */
    public boolean enabled(Game game) {
        if (condition != null) {
            return condition.evaluate(game);
        }
        return true;
    }

    /**
     * Exécute les actions associées à cette action.
     * Les actions portent sur le contexte passé en paramètre
     * @param game Le contexte
     */
    public void execute(Game game) {
        if (action != null) {
            action.execute(game);
        }
    }

    // Getters

    public String getLibelle() {
        return libelle;
    }

    public String getNext() {
        return next;
    }

    public String toString() {
        return libelle;
    }
}
