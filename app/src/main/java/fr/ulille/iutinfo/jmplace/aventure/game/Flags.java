package fr.ulille.iutinfo.jmplace.aventure.game;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;
import java.util.HashSet;

public class Flags implements Parcelable {
    private HashSet flags;
    public static Creator<Flags> CREATOR = new Creator<Flags>(){

        @Override
        public Flags createFromParcel(Parcel parcel) {
            return new Flags(parcel);
        }

        @Override
        public Flags[] newArray(int i) {
            return new Flags[i];
        }
    };

    Flags() {
        flags = new HashSet<>();
    }

    private Flags(Parcel parcel) {
        String[] array = new String[0];
        parcel.readStringArray(array);
        flags = new HashSet(Arrays.asList(array));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray((String[]) flags.toArray());
    }

    public void set(String flag) {
        flags.add(flag);
    }

    public void clear(String flag) {
        flags.remove(flag);
    }

    public boolean test(String flag) {
        return flags.contains(flag);
    }
}
