package fr.ulille.iutinfo.jmplace.aventure.modele;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fr.ulille.iutinfo.jmplace.aventure.game.Game;


public class Action {
    private String[] setFlags;
    private String[] clearFlags;

    public Action(JSONObject json) {
        Object set;
        try {
            set = json.get("set");
            if (set instanceof String) {
                setFlags = new String[] { (String) set };
            } else if (set instanceof JSONArray) {
                JSONArray arraySet = (JSONArray) set;
                setFlags = new String[arraySet.length()];
                for (int i = 0; i < arraySet.length(); i++) {
                    setFlags[i] = arraySet.getString(i);
                }
            }
        } catch (JSONException e) {
            setFlags = new String[0];
        }

        Object clear;
        try {
            clear = json.get("clear");
            if (clear instanceof String) {
                clearFlags = new String[] { (String) clear };
            } else if (clear instanceof JSONArray) {
                JSONArray arrayClear = (JSONArray) clear;
                clearFlags = new String[arrayClear.length()];
                for (int i = 0; i < arrayClear.length(); i++) {
                    clearFlags[i] = arrayClear.getString(i);
                }
            }
        } catch (JSONException e) {
            clearFlags = new String[0];
        }
    }

    void execute(Game game) {
        for (String toSet : setFlags) {
            game.set(toSet);
        }
        for (String toClear : clearFlags) {
            game.clear(toClear);
        }
    }
}
