package fr.ulille.iutinfo.jmplace.aventure.modele;

import static fr.ulille.iutinfo.jmplace.aventure.modele.JsonKeywords.KEY_ETATS;
import static fr.ulille.iutinfo.jmplace.aventure.modele.JsonKeywords.KEY_TITLE;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import fr.ulille.iutinfo.jmplace.aventure.R;


public class Aventure {
    // Constantes utilisées pour la sauvegarde

    private String title;
    private Map<String, Page> pages;

    /**
     * Construction de l'aventure à partir de l'objet Json
     * @param json La description de l'aventure obtenu après analyse du json
     * @throws JSONException si la description ne respecte pas la syntaxe JSON
     */
    private Aventure(JSONObject json) throws JSONException {
        title = json.getString(KEY_TITLE);
        pages = new HashMap<>();
        JSONArray array = json.getJSONArray(KEY_ETATS);
        for (int i = 0 ; i < array.length(); i++) {
            Page page = new Page(array.getJSONObject(i));
            pages.put(page.getId(), page);
        }
    }

    /**
     * Récupération de la description textuelle depuis la ressource R.raw.premiers_pas
     * @param context Le contexte (activity courante)
     * @return La description de l'avneture sous forme de Chaîne (StringBuilder)
     */
    private static StringBuilder getConfig(Context context) {
        StringBuilder text = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.premiers_pas)));
        try {
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
            return text;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    /**
     * Initialisation de l'aventure depuis la chaîne json passée en paramètre
     * @param jsonString La chaîne à analyser
     * @return L'aventure après construction
     * @throws JSONException La chaîne n'est pas syntaxiquement correcte.
     */
    public static Aventure parseConfig(String jsonString) throws JSONException {
        JSONObject json =  new JSONObject(jsonString);
        return  new Aventure(json);
    }

     /**
     * Initialisation de l'aventure depuis la ressource R.raw.premiers_pas
     * @param context Le contexte android
     * @return L'aventure après construction
     * @throws JSONException La chaîne n'est pas syntaxiquement correcte.
     */
    public static Aventure parseConfig(Context context) throws JSONException {
        StringBuilder text = getConfig(context) ;
        JSONObject json =  new JSONObject(text.toString());
        return  parseConfig(text.toString());
    }

    /**
     * Obtient la page de l'aventure d'après sa référence
     * @param pageRef La référence (chaîne de caractères)
     * @return La page ciblée
     */
    public Page getPage(String pageRef) {
        return pages.get(pageRef);
    }

    /**
     * Obrtient le titre de l'aventure
     * @return Le tire (chaîne de caractères)
     */
    public String getTitre() {
        return title;
    }
}
