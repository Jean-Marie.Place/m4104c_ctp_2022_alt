package fr.ulille.iutinfo.jmplace.aventure.game;

import android.os.Bundle;
import android.util.Log;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;


/**
 * Objets décrivant le contexte:
 * <ul>
 *     <li>La référence de la page courante</li>
 *     <li>L'ensemble des indicateurs actifs</li>
 * </ul>
 */
public class Game {
    private static final String KEY_FLAGS = "FLAGS";
    private static final String KEY_PAGE = "PAGE";
    private static final String KEY_START = "START_DATE";

    private String pageRef;
    private HashSet flags;
    private Date startDate;

    /**
     * Initialise le contexte à partir d'un 'paquet' de sauvegarde.
     * Attributs à sauvegarder:
     * pageRef
     * startDate
     * flags
     * @param savedInstanceState le paquet de sauvegarde
     */
    public Game(Bundle savedInstanceState) {
        // TODO Etape 1
    }

    /**
     * Sauvergarde l'état du jeu dans un paquet
     * @param bundle le paquet où les données seriont sauvegardées
     */
    public void save(Bundle bundle) {
        // TODO Etape 1
    }

    /**
     * Initialise un début de partie:
     * <ul>
     *     <li>pageRef => "Start"</li>
     *     <li>Set d'indicateurs (flags) vidé</li>
     * </ul>
     */
    public Game() {
        pageRef = "Start";
        flags = new HashSet<>();
        startDate = new Date();
    }

    /**
     * Obtient la référence vers la page en cours
     * @return la référence (String)
     */
    public String getPageRef() {
        return pageRef;
    }

    /**
     * Modifie la référence vers la page courante = change de page
     * @param next La référence (String)
     */
    public void setPageRef(String next) {
        pageRef = next;
    }

    /**
     * Active un drapeau
     * @param flag Le nom du drapeau activé
     */
    public void set(String flag) {
        flags.add(flag);
    }

    /**
     * Désactive un drapeau
     * @param flag Le drapeau désactivé
     */
    public void clear(String flag) {
        flags.remove(flag);
    }

    /**
     * Teste l'état d'un drapeau
     * @param flag Le drapeau testé.
     * @return
     */
    public boolean test(String flag) {
        return flags.contains(flag);
    }

    public long getScore() {
        Date now = new Date();
        return now.getTime() - startDate.getTime();
    }
}
