package fr.ulille.iutinfo.jmplace.aventure.modele;

import static fr.ulille.iutinfo.jmplace.aventure.modele.JsonKeywords.KEY_CHOIX;
import static fr.ulille.iutinfo.jmplace.aventure.modele.JsonKeywords.KEY_DESCRIPTION;
import static fr.ulille.iutinfo.jmplace.aventure.modele.JsonKeywords.KEY_ID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.ulille.iutinfo.jmplace.aventure.game.Game;

public class Page {
    private String id;
    private String[] description;
    private Choix[] choix;

    /**
     * Construction d'une page à partir de sa description JSON
     * @param json la description
     * @throws JSONException Si la description n'est pas correcte
     */
    public Page(JSONObject json) throws JSONException {
        // Identifiant de la page
        id = json.getString(KEY_ID);

        // Description (sous forme de tableau de chaînes de caractères
        JSONArray arrayD = json.getJSONArray(KEY_DESCRIPTION);
        description = new String[arrayD.length()];
        for (int i = 0; i < arrayD.length(); i++) {
            description[i] = arrayD.getString(i);
        }

        // Liste des choix (sous forme de tableau)
        JSONArray arrayC = json.getJSONArray(KEY_CHOIX);
        choix = new Choix[arrayC.length()];
        for (int i = 0; i < arrayC.length(); i++) {
            choix[i] = new Choix(arrayC.getJSONObject(i));
        }
    }

    // Getters

    public String getId() {
        return id;
    }

    public String[] getDescription() {
        return description;
    }

    public Choix[] getChoix() {
        return choix;
    }

    public ArrayList<Choix> getActiveChoices(Game game) {
        ArrayList<Choix> returned = new ArrayList<>();
        for (Choix c : choix) {
            if (c.enabled(game)) {
                returned.add(c);
            }
        }
        return returned;
    }
}
