package fr.ulille.iutinfo.jmplace.aventure.modele;

import fr.ulille.iutinfo.jmplace.aventure.game.Game;

public class ConditionFlag implements Condition {
    String testedFlag;

    public ConditionFlag(Object object) {
            if (object instanceof String) {
                testedFlag = (String) object;
            }
    }

    @Override
    public boolean evaluate(Game game) {
        return game.test(testedFlag);
    }
}
