package fr.ulille.iutinfo.jmplace.aventure;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;

import fr.ulille.iutinfo.jmplace.aventure.game.Game;
import fr.ulille.iutinfo.jmplace.aventure.modele.Aventure;
import fr.ulille.iutinfo.jmplace.aventure.modele.Choix;
import fr.ulille.iutinfo.jmplace.aventure.modele.Page;



public class MainActivity extends AppCompatActivity {
    public static final String LOG_TAG = "APPLI";
    public static final String WIN_PAGE = "WIN";
    public static final String GAME_OVER_PAGE = "GAME OVER";
    public static final String KEY_DOOM = "DOOM";
    public static final String KEY_SCORE = "SCORE";
    public final static int REQUEST_CODE = 1;

    private Aventure aventure;
    private Game game;

    private TextView tvDescription;
    private TextView tvTitre;
    private Button[] buttons;
    private ListView lvChooser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            aventure = Aventure.parseConfig(this);
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Erreur de lecture du fichier aventure");
            e.printStackTrace();
        }

        // TODO Etape 1
        restartAdventure();

        tvDescription = findViewById(R.id.description);
        tvTitre = findViewById(R.id.tvTitre);
        buttons = new Button[3];
        buttons[0] = findViewById(R.id.button_0);
        buttons[1] = findViewById(R.id.button_1);
        buttons[2] = findViewById(R.id.button_2);
        lvChooser = findViewById(R.id.lvChooser);

        tvTitre.setText(aventure.getTitre());
        // TODO Etape 2b
        display();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        // TODO Etape 1
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void doChoose(View view) {
        doChoose((Button) view);
    }

    private void restartAdventure() {
        Log.d(LOG_TAG, "Restart aventure (begin)");
        game = new Game();
        Log.d(LOG_TAG, "Restart aventure (end)");
    }

    private Page getPage() {
        return aventure.getPage(game.getPageRef());
    }

    private void doAction(Choix choix) {
        String etiquette = choix.getNext();
        if ((etiquette.equals(GAME_OVER_PAGE) || (etiquette.equals(WIN_PAGE)))) {
            gotoEndPage(etiquette);
        } else{
            Page next = aventure.getPage(etiquette);
            game.setPageRef(etiquette);
            choix.execute(game);
            display();
        }
    }

    private void doChoose(Button button) {
        Choix choix = (Choix) button.getTag();
        doAction(choix);
    }

    private void gotoEndPage(String etiquette) {
        // TODO Etape 3
    }

    // TODO Etape 3

    private void display() {
        Log.d(LOG_TAG, "Page :" + game.getPageRef());
        Page currentPage = getPage();
        if (currentPage == null) {
            Toast.makeText(this, R.string.error_pending, Toast.LENGTH_LONG).show();
            restartAdventure();
            currentPage = getPage();
        }
        String description = "";
        for (String ligne : currentPage.getDescription()) {
            description += ligne.replace("\\n", "\n");
        }
        tvDescription.setText(description);
        int idxBouton = 0;
        final ArrayList<Choix> enabled = currentPage.getActiveChoices(game);
        if (enabled.size() > 3) {
            // TODO Etape 2a
            lvChooser.setVisibility(View.VISIBLE);
        } else {
            for (Choix choix : enabled) {   // tout afficher sauf la listView
                lvChooser.setVisibility(View.GONE);
                buttons[idxBouton].setText(choix.getLibelle());
                buttons[idxBouton].setVisibility(View.VISIBLE);
                buttons[idxBouton].setTag(choix);
                idxBouton++;
            }
        }
        // TODO Etape 0.1 cacher les boutons inutilisés (setVisibility)
    }
}