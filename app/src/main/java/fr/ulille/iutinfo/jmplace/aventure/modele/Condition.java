package fr.ulille.iutinfo.jmplace.aventure.modele;

import fr.ulille.iutinfo.jmplace.aventure.game.Game;

public interface Condition {
    boolean evaluate(Game game);
}
