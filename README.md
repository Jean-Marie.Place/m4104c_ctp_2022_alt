# TP de révision

Ce contrôle TP est d'une durée de 2h. L’usage des documents papier et d'internet est
autorisé. L’usage d’un ordinateur personnel et/ou d'un appareil android personnel est
également autorisé. Attention toute modification de votre dépôt strictement postérieure à
l'heure limite de la remise sera ignorée. Vous devez bien entendu soumettre un travail
original, dont vous êtes l'unique auteur. Barème indicatif susceptible d'être ajusté.

## Mise en place de votre projet (et enregistrement)

Récupérez le code fourni sur le dépôt git dont l'URL est:

1. IMPORTANT : ‘forkez’ le dépôt (si problème : avez-vous atteint le nombre maximal
de projets ?)
2. ajoutez immédiatement votre enseignant au dépôt nouvellement créé.
3. Importez le projet sous androidStudio
(Menu File → New → Project from versioncontrol → Git → …)

A la fin de chaque question et à la fin du TP, validez votre travail et poussez le sur le
serveur :
(menu : Git > Commit > `{édition du commit message}` > Commit and push (plutôt que
Commit)
Vous pouvez aussi utilisez les commandes git standard du shell si vous le souhaitez.
Les endroits où vous devrez intervenir sont annotés par les marques TODO
Le menu View > Tool Window > TODO vous ouvre une fenêtre qui permet de naviguer sur ces
marques.

## Description de l’application

Le thème de cette partie est le développement d'un jeu d'aventure, dont l'utilisateur est le
héros. Le jeu se compose de différentes pages, au cours desquelles l'utilisateur doit faire
des choix, qui déterminent les prochaines étapes.

Une application complète aventure.apk vous est livrée elle préfigure une partie de ce
que pourrait donner le résultat de votre contrôle TP. Pas de panique, nous vous donnons
les éléments les plus longs à développer.

Le code source d'une implémentation de base vous est fourni à titre d'exemple, et il vous
est demandé d'apporter un certain nombre de modifications.

L'implémentation qui vous est fournie est (volontairement) atypique : tout se passe dans
une unique activité – seuls le texte et la visibilité des widgets sont modifiés pour enchaîner
les pages.

_Note_: Il s'agit de la reprise d'un ancienne application android. Il vous sera également demandé
de réactualiser l'architecture du programme.

## Architecture existante

Le logiciel tel qu’il vous est fourni comporte deux packages. Décrit ci-après. Sauf indication
contraire, vous n’aurez pas à modifier les classes proposées, l’essentiel de votre travail se
fera dans les classes liées aux activités.

### Le package aventure
Ce package a pour objet de décrire la trame complète de l’histoire. Tous les objets de ce
paquetage sont constants et réinitialisés au démarrage de l’application (Il sera donc inutile
de les sauvegarder). L’histoire est obtenue par lecture d’un fichier texte inclus dans les
ressources (res/raw/premiers_pas). Il s’agit d’une description JSON des objets
suivants :

* Aventure : L’aventure décrit la totalité de l'histoire. Elle comporte un titre et une liste de
pages. Chaque page est repérée par un identifiant unique (une chaîne de caractères). Les
références suivantes sont réservées :
    *  « Start » désigne le point d’entrée de l’histoire.
    *  « WIN » indique un fin victorieuse pour le joueur.
    *  « GAME OVER » décrit une fin méprisable et abjecte pour l’aventurier.

* Page : Une page décrit une étape à laquelle un joueur peut arriver. Elle est
essentiellement constituée :
    * D’une description textuelle qui décrit l'environnement du joueur. Cette description prend plusieurs lignes.
    * D’une ou plusieurs possibilités d’interaction (prendre un objet, choisir un chemin, etc..).
     Chaque possibilité est appelée choix. L’application proposera à l’utilisateur les 
     choix possibles compte-tenu de l’état du jeu.
* Choix : Un choix est décrit par :
    * Un libellé (texte de la proposition).
    * La référence de la page atteinte si ce choix est sélectionné.
    * Optionnellement, on peut lier à un choix une action sur l’environnement (acquérir un objet, marquer le passage à un endroit).
    * Optionnellement, un choix peut aussi être conditionné à un état de l’environnement (possède t-on tel objet ? est-t-on déjà passé par là ?)

### Le package game
Ce package concentre tous les éléments qui peuvent évoluer tout au long du parcours. Ils
sont au nombre de trois (tous deux attributs de la classe Game)
* La chaîne pageRef qui désigne la page actuelle.
* Un ensemble de drapeaux (aka indicateurs) flags. Un indicateur est identifié par
une chaîne de caractères. Un drapeau est levé par une action liée à un choix et
peut être testé lors de l’évaluation d’une condition sur un choix.
* La date de départ de l’aventure. Ceci permet d’établir un score pour le joueur)

## Fonctionnement global

### Programme
* L’Aventure est initialisée (cf ressource JSON)
* L’état du jeu est initialisé (pageRef = Start, flags = vide, Date de départ enregistrée }
* Boucle tant que l’on est pas sur WIN ou GAME_OVER
* Affichage du texte et mise à jour des boutons (`display()`)

### Sur action bouton.
* Calculer et affecter la nouvelle valeur de pageRef
* _Note_: le choix associé au bouton est inscrit dans l’attribut tag du bouton (et donc sauvegardé automatiquement avec le bouton lors des changements de configuration)
* display (mise à jour de l’affichage en fonction de l’état actuel)

### Display:
* Afficher la description de la page dans la zone de texte
*  Construire la liste des choix possibles : `enable = currentPage.getActiveChoices(game)`
*  Pour chaque choix posssible:
*  Affecter le choix à un bouton.
*  Masquer (`setVisibility(GONE)`) les boutons non utilisés

## Travail à faire

### Etape 1 mémoriser l'état du jeu

Dans cette première étape, on s'intéresse à la mémorisation de l’état de l’application. Par
exemple, si le joueur a obtenu un accessoire à une étape donnée, l'interface doit lui
permettre de s'en servir à un moment opportun. (rappel : tout est stocké dans la classe
Game)

#### Question 1. Créez une classe Application et transferez-y les attribut `aventure`et `game` de l'activité principale.

3 sous-tâches:

1 Créer la classe application avec ses attributs et ses accesseurs.

2 Transférez l'initialisation de cette classe depuis une partie du code `MainActivity`

3 Ajustez le code de `MainActivity` pour utiliser les accesseur de la classe créée.

### Etape 2. Pour plus de 4 choix, on utilisera une liste.

Dans cette deuxième étape, on s'intéresse à la possibilité d'utiliser une listView pour toutes
les situations où l'utilisateur a plus de 3 choix (4 et plus). Pour faire vos essais, vous pouvez ajouter un choix sur l'élément racine du jeux dans la description json (`res/raw/premiers_pas`)

La listView `lvChooser` est déjà fournie dans l'interface. mais elle est deprecated. Il faut la remplacer par une RecyclerView.
les textes à ajouter à la liste. Il faudra aussi gérer la visibilité des boutons et de la liste selon le nombre
de choix de l'étape courante du jeu.

#### Question 2. Faire de `lvChooser` une RecyclerView dans le layout de l'activité.

#### Question 3: Installez le recyclerView dans l'activité principale.
_Note:_: Vous n'avez pas à redessiner le layout d'un élément simple. 
il existe des layout préfédénis dans android: `android.R.layout.simple_list_item_1` suffira. Dans ce layout, le textView `text1`donne le contenu de l'élément.

#### Question 3:  Interaction
Ajoutez ce qu'il faut pour qu'un clic simple sur un élément de liste permette de choisir l'élément ciblé.

### Etape 3: Ecran final

Lorsque le jeu se termine, nous voulons afficher une image de victoire ou de défaite,
dépendant de l'issue de la partie.
L’activité correspondante est déjà préparée sous le nom (EndScreen). Vous aurez :
* A transmettre à cette activité l’info (réussite ou échec sous la forme qui vous plaira)
* Selon cette valeur afficher l’image qui convient (utilisez les images ressources (`res/drawable/victory.png`) et (`res/drawable/defeat.png`)
* proposer à l’utilisateur, soit de recommencer, soit de quitter l ‘application
* De gérer ce retour dans l’activité principale

Ajoutez cette fonctionnalité dans une nouvelle activité.
4 sous-tâches :
* Lancer l’écran final en lui transmettant le verdict et le score (temps réalisé)
* Initialiser l’image de l’écran final en fonction du verdict transmis
* Retourner le résultat du choix de continuation
* Appliquer ce choix dans l’activité principale

Cette question faite, commitez vos modifications sous le message ‘Etape 3’.


